﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;

namespace SnakeGame
{

    class Food
    {
        public Shape foodElement { get; set; }
        public Point position { get; set; }


        protected void Init(Brush foodColor)
        {
            this.foodElement.Width = Settings.foodSize;
            this.foodElement.Height = Settings.foodSize;
            this.foodElement.Fill = foodColor;
            this.DrawFoodElement();
        }


        //private Point NewFoodPosition(Canvas screen)
        //{
        //    Point randFoodPosition = new Point();

        //    randFoodPosition.X = random.Next(5, (int)screen.Width - 50);
        //    randFoodPosition.Y = random.Next(5, (int)screen.Height - 50);

        //    return randFoodPosition;
        //}


        public virtual void DrawFoodElement()
        {
            Canvas.SetLeft(this.foodElement, this.position.X);
            Canvas.SetTop(this.foodElement, this.position.Y);
        }

        public UIElement FeedBack()
        {
            return foodElement;
        }
    }

}