﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;


namespace SnakeGame
{
    class PlusFood : Food//, IFood
    {
        
        public PlusFood(Point position)
        {
            foodElement = new Ellipse();
            this.position = position;
            Init(Settings.plusFoodColor);
        }

        public void myScore()
        {
            Settings.score += 10;
        }

        //public override void drawFoodElement()
        //{
        //    throw new NotImplementedException();
        //}

        //public abstract void generateFood();

       
    }
}
