﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;


namespace SnakeGame
{
    class MinusFood : Food//, IFood
    {
        
        public MinusFood(Point position)
        {
            foodElement = new Rectangle();
            this.position = position;
            Init(Settings.minusFoodColor);
        }


        public void myScore()
        {
            Settings.score -= 10;
        }
    }
}
