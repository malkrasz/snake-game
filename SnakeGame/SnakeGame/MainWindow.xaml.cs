﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.IO;
using System.IO.IsolatedStorage;




namespace SnakeGame
{

    public partial class MainWindow : Window
    {

        private TimeSpan FAST = new TimeSpan(1);
        private TimeSpan MODERATE = new TimeSpan(10000);
        private TimeSpan SLOW = new TimeSpan(50000);
        private TimeSpan DAMNSLOW = new TimeSpan(500000);


        private List<PlusFood> plusFood = new List<PlusFood>();
        private List<MinusFood> minusFood = new List<MinusFood>();
       // private List<Food> minusFood = new List<Food>();
        private List<Point> snakeBody = new List<Point>();
        private Point currentPosition;
        private Random random = new Random();
        private DispatcherTimer gameTimer;



        public MainWindow()
        {
            InitializeComponent();
            StartGame();
        }


        private void StartGame()
        {
            snakeBody.Clear();
            plusFood.Clear();
            minusFood.Clear();
            myCanvas.Children.Clear();
            new Settings();
            Timer();
            Paint();
            currentPosition = Settings.head;
        }

        private void Timer()
        {
            gameTimer = new DispatcherTimer();
            gameTimer.Tick += UpdateWindow;
            gameTimer.Interval = SLOW;
            gameTimer.Start();

        }

        private void Paint()
        {
            DrawSnake(Settings.head);

            for (int i = 0; i < 5; i++)
            {
               // CreatePlusFood(i);
               // DrawPlusFood(i, plusFood[i]);
                plusFood.Insert(i, new PlusFood(NewFoodPosition(myCanvas)));
                myCanvas.Children.Insert(i, plusFood[i].FeedBack());

               
               // CreateMinusFood(i);
                minusFood.Insert(i, new MinusFood(NewFoodPosition(myCanvas)));
                myCanvas.Children.Insert(i, minusFood[i].FeedBack());
                //DrawMinusFood(i, minusFood[i]);
            }
        }


        private void DrawSnake(Point actualPosition)
        {
            Rectangle bodyPart = new Rectangle();
            bodyPart.Fill = Settings.snakeColor;
            bodyPart.Width = bodyPart.Height = Settings.snakeSize;

            Canvas.SetTop(bodyPart, actualPosition.Y);
            Canvas.SetLeft(bodyPart, actualPosition.X);
            myCanvas.Children.Add(bodyPart);
            snakeBody.Add(actualPosition);

            int elementsCount = myCanvas.Children.Count;

            // Restrict the tail of the snake
            if (elementsCount > Settings.snakeLength)
            {
                myCanvas.Children.RemoveAt(elementsCount - Settings.snakeLength + plusFood.Count + minusFood.Count - 1);
                snakeBody.RemoveAt(elementsCount - Settings.snakeLength);
            }

            if (Settings.score < 0) Settings.score = 0;
            lblScore.Content = "Score: " + Settings.score.ToString();
        }




        private Point NewFoodPosition(Canvas screen)
        {
            Point randFoodPosition = new Point();

            randFoodPosition.X = random.Next(5, (int)screen.Width - 50);
            randFoodPosition.Y = random.Next(5, (int)screen.Height - 50);

            return randFoodPosition;
        }


        private Point GenerateFood()
        {
            int rndX, rndY;
            Point rnd = new Point();

            do
            {
                rndX = random.Next(5, (int)myCanvas.Width - 50);
                rndY = random.Next(5, (int)myCanvas.Height - 50);
            } while ((Math.Abs(rndX - currentPosition.X) < Settings.snakeSize) &&
                        (Math.Abs(rndY - currentPosition.Y) < Settings.snakeSize));

            rnd.X = rndX;
            rnd.Y = rndY;

            return rnd;
        }



       

        private void UpdateWindow(object sender, EventArgs e)
        {
            
            if (Input.Pressed(Key.Escape))
                Close();

            
            if (Settings.gameOver)
            {
                gameTimer.Stop();
                GameOver();
            }

            
            else
            {
                if (Input.Pressed(Key.Down) && Settings.direction != moveDirection.up)
                    Settings.direction = moveDirection.down;

                else if (Input.Pressed(Key.Up) && Settings.direction != moveDirection.down)
                    Settings.direction = moveDirection.up;

                else if (Input.Pressed(Key.Right) && Settings.direction != moveDirection.left)
                    Settings.direction = moveDirection.right;

                else if (Input.Pressed(Key.Left) && Settings.direction != moveDirection.right)
                    Settings.direction = moveDirection.left;

                UpdateSnake();
            }

        }



        private void UpdateSnake()
        {

            switch (Settings.direction)
            {
                case moveDirection.down:
                    currentPosition.Y++;
                    DrawSnake(currentPosition);
                    break;
                case moveDirection.up:
                    currentPosition.Y--;
                    DrawSnake(currentPosition);
                    break;
                case moveDirection.right:
                    currentPosition.X++;
                    DrawSnake(currentPosition);
                    break;
                case moveDirection.left:
                    currentPosition.X--;
                    DrawSnake(currentPosition);
                    break;
            }




            if ((currentPosition.X < 0) || (currentPosition.X > myCanvas.Width) ||
                (currentPosition.Y < 0) || (currentPosition.Y > myCanvas.Height))
                Settings.gameOver = true;



            int n = 0, x = 0;

            foreach (var foodPiece in plusFood)
            {
                if ((Math.Abs(foodPiece.position.X - currentPosition.X) < Settings.snakeSize) &&
                    (Math.Abs(foodPiece.position.Y - currentPosition.Y) < Settings.snakeSize))
                {
                    Settings.snakeLength += Settings.snakeSize;
                    foodPiece.myScore();

                    x = myCanvas.Children.IndexOf(foodPiece.FeedBack());
                    n = plusFood.IndexOf(foodPiece);
                    myCanvas.Children.RemoveAt(x);
                    plusFood.RemoveAt(n);
                    plusFood.Insert(n, new PlusFood(NewFoodPosition(myCanvas)));
                    myCanvas.Children.Insert(n, plusFood[n].FeedBack());

                    break;
                }
            }



            foreach (var foodPiece in minusFood)
            {
                if ((Math.Abs(foodPiece.position.X - currentPosition.X) < Settings.snakeSize) &&
                    (Math.Abs(foodPiece.position.Y - currentPosition.Y) < Settings.snakeSize))
                {
                    Settings.snakeLength += Settings.snakeSize;
                    foodPiece.myScore();

                    x = myCanvas.Children.IndexOf(foodPiece.FeedBack());
                    n = minusFood.IndexOf(foodPiece);
                    myCanvas.Children.RemoveAt(x);
                    minusFood.RemoveAt(n);
                    minusFood.Insert(n, new MinusFood(NewFoodPosition(myCanvas)));
                    myCanvas.Children.Insert(n, minusFood[n].FeedBack());
                    //CreateMinusFood(n);
                    //DrawMinusFood(n, minusFood[n]);

                    break;
                }
            }


            for (int a = 0; a < (snakeBody.Count - 2 * Settings.snakeSize); a++)
            {
                if ((Math.Abs(snakeBody[a].X - currentPosition.X) < (Settings.snakeSize)) &&
                    (Math.Abs(snakeBody[a].Y - currentPosition.Y) < (Settings.snakeSize)))
                {
                    Settings.gameOver = true;
                }
            }
        }



        private void KeyPressed(object sender, KeyEventArgs e)
        {
            Input.StateOfKey(e.Key, true);
        }



        private void KeyReleased(object sender, KeyEventArgs e)
        {
            Input.StateOfKey(e.Key, false);
        }


        private void SaveScore()
        {
            string folderName = @"c:\ScoreFolder";
            string pathString = System.IO.Path.Combine(folderName, "SubFolder");
            System.IO.Directory.CreateDirectory(pathString);
            string fileName = "MyNewFile.txt";
            pathString = System.IO.Path.Combine(pathString, fileName);
            Console.WriteLine("Path to my file: {0}\n", pathString);


            using (System.IO.FileStream fs = new System.IO.FileStream(pathString, FileMode.Append))
                fs.WriteByte((byte)Settings.score);

            try
            {
                byte[] readBuffer = System.IO.File.ReadAllBytes(pathString);
                foreach (byte b in readBuffer)
                {
                    Console.Write(b + "\n");
                }
                Console.WriteLine();
            }
            catch (System.IO.IOException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }


        private void GameOver()
        {
            if (Settings.score < 0) Settings.score = 0;
            SaveScore();

            MessageBox.Show("You Lose! Your score is " + Settings.score.ToString() + "\nPress enter to play again.", "Game Over", MessageBoxButton.OK, MessageBoxImage.Hand);
            StartGame();
        }


    }
}