

	import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
	import com.sun.j3d.utils.geometry.*;
	import com.sun.j3d.utils.picking.PickCanvas;
	import com.sun.j3d.utils.picking.PickResult;
	import com.sun.j3d.utils.picking.PickTool;
	import javax.media.j3d.*;
	import javax.swing.*;
	import java.awt.*;
	import com.sun.j3d.utils.universe.SimpleUniverse;
	import java.awt.event.KeyEvent;
	import java.awt.event.KeyListener;
	import java.awt.event.MouseAdapter;
	import java.awt.event.MouseEvent;
	import java.awt.event.MouseListener;
	import java.awt.event.WindowAdapter;
	import java.awt.event.WindowEvent;
	import java.util.ArrayList;
	import java.util.Enumeration;
	import java.util.Vector;
	import javax.media.j3d.Transform3D;
	import javax.vecmath.AxisAngle4d;
	import javax.vecmath.Color3f;
	import javax.vecmath.Point3d;
	import javax.vecmath.Point3f;
	import javax.vecmath.Vector3d;
	import javax.vecmath.Vector3f;
	 
	 
	
	 
		
	 
	 
	
	public class rubikCube extends MouseAdapter implements KeyListener
	{
	 
		int[][][] kostka = new int[3][3][3];
	 
		private PickCanvas pickCanvas;
		private Transform3D rotate1 = new Transform3D();
		BranchGroup scena = new BranchGroup();
		BranchGroup root;
		TransformGroup warstwa = new TransformGroup();
		public boolean mouseIsMoving = false;
		double kat =0;
		double kat2 =0;
		double kat3 =0;
		boolean zmin = false;
		SimpleUniverse simpleU;
		ArrayList<TransformGroup> kosteczki = new ArrayList<TransformGroup>();
	 
	 
	 
		public class Point3i
		{
			public int x;
			public int y;
			public int z;
			public Point3i()
			{
				x = 0;
				y = 0;
			}
			public Point3i(int x, int y, int z)
			{
				this.x = x;
				this.y = y;
				this.z = z;
			}
		};
	 
		public void Obrot(String akcja)
		{
			Point iteratorFrom = new Point();
			Point iteratorTo = new Point();
			Point From = new Point();
			Point To = new Point();
			Point3i start = new Point3i();
			Point3i stop = new Point3i();
	 
			if (akcja == "R")
			{
				start = new Point3i(2, 0, 0);
				stop = new Point3i(2, 0, 2);
	 
				From.x = start.y;
				From.y = start.z;
	 
				To.x = stop.y;
				To.y = stop.z;
			}

			else if (akcja == "R'")
			{
				start = new Point3i(2, 0, 0);
				stop = new Point3i(2, 0, 2);
	 
				From.x = stop.y;
				From.y = stop.z;
	 
				To.x = start.y;
				To.y = start.z;
			}
			else if (akcja == "L")
			{
				start = new Point3i(0, 0, 0);
				stop = new Point3i(0, 2, 0);
	 
				From.x = start.y;
				From.y = start.z;
	 
				To.x = stop.y;
				To.y = stop.z;
			}
			else if (akcja == "L'")
			{
				start = new Point3i(0, 0, 0);
				stop = new Point3i(0, 2, 0);
	 
				From.x = stop.y;
				From.y = stop.z;
		
				To.x = start.y;
				To.y = start.z;
	 
			}
			else if (akcja == "U")
			{
				start = new Point3i(0, 0, 2);
				stop = new Point3i(0, 2, 2);
	 
				From.x = start.x;
				From.y = start.y;
	 
				To.x = stop.x;
				To.y = stop.y;
			}
			else if (akcja == "U'")
			{
				start = new Point3i(0, 0, 2);
				stop = new Point3i(0, 2, 2);
	 
				From.x = stop.x;
				From.y = stop.y;
				
				To.x = start.x;
				To.y = start.y;
	 
			}
			else if (akcja == "D")
			{
				start = new Point3i(0, 0, 0);
				stop = new Point3i(2, 0, 0);
	 
				From.x = start.x;
				From.y = start.y;
	 
				To.x = stop.x;
				To.y = stop.y;
			}
			else if (akcja == "D'")
			{
				start = new Point3i(0, 0, 0);
				stop = new Point3i(2, 0, 0);
	 
				From.x = stop.x;
				From.y = stop.y;
	 
				To.x = start.x;
				To.y = start.y;
			}
			else if (akcja == "F")
			{
				start = new Point3i(0, 0, 0);
				stop = new Point3i(0, 0, 2);
	 
				From.x = start.x;
				From.y = start.z;
	 
				To.x = stop.x;
				To.y = stop.z;
			}
			else if (akcja == "F'")
			{
				start = new Point3i(0, 0, 0);
				stop = new Point3i(0, 0, 2);
   
				From.x = stop.x;
				From.y = stop.z;
	 
				To.x = start.x;
				To.y = start.z;
			}
			else if (akcja == "B")
			{
				start = new Point3i(0, 2, 0);
				stop = new Point3i(2, 2, 0);
	 
				From.x = start.x;
				From.y = start.z;
	 
				To.x = stop.x;
				To.y = stop.z;
			}
			else if (akcja == "B'")
			{
				start = new Point3i(0, 2, 0);
				stop = new Point3i(2, 2, 0);
	 
				From.x = stop.x;
				From.y = stop.z;
	 
				To.x = start.x;
				To.y = start.z;
			}
	 

			int[][] tempWarstwa = new int[3][3];

			for (int i = 0; i < 8; i++)
			{
				if (akcja == "R" | akcja == "R'" | akcja == "L" | akcja == "L'") tempWarstwa[To.x][To.y] = kostka[start.x][From.x][From.y];
				if (akcja == "U" | akcja == "U'" | akcja == "D" | akcja == "D'") tempWarstwa[To.x][To.y] = kostka[From.x][From.y][start.z];
				if (akcja == "F" | akcja == "F'" | akcja == "B" | akcja == "B'") tempWarstwa[To.x][To.y] = kostka[From.x][start.y][From.y];

				if (From.x == 0 & From.y == 0)
				{
					iteratorFrom.x = 0;
					iteratorFrom.y = 1;
				}
				if (From.x == 0 & From.y == 2)
				{
					iteratorFrom.x = 1;
					iteratorFrom.y = 0;
				}
				if (From.x == 2 & From.y == 2)
				{
					iteratorFrom.x = 0;
					iteratorFrom.y = -1;
				} 
				if (From.x == 2 & From.y == 0)
				{
					iteratorFrom.x = -1;
					iteratorFrom.y = 0;
				}

				From.x += iteratorFrom.x;
				From.y += iteratorFrom.y;
	 
				if (To.x == 0 & To.y == 0)
				{
					iteratorTo.x = 0;
					iteratorTo.y = 1;
				}
				if (To.x == 0 & To.y == 2)
				{
					iteratorTo.x = 1;
					iteratorTo.y = 0;
				}
				if (To.x == 2 & To.y == 2)
				{
					iteratorTo.x = 0;
					iteratorTo.y = -1;
				}
				if (To.x == 2 & To.y == 0)
				{
					iteratorTo.x = -1;
					iteratorTo.y = 0;
				}

				To.x += iteratorTo.x;
				To.y += iteratorTo.y;
	 
			}
	 
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					if (i == 1 & j == 1) continue;
					if (akcja == "R" | akcja == "R'" | akcja == "L" | akcja == "L'") kostka[start.x][i][j] = tempWarstwa[i][j];
					if (akcja == "U" | akcja == "U'" | akcja == "D" | akcja == "D'") kostka[i][j][start.z] = tempWarstwa[i][j];
					if (akcja == "F" | akcja == "F'" | akcja == "B" | akcja == "B'") kostka[i][start.y][j] = tempWarstwa[i][j];
				}
			}
		}
	 

	 
		rubikCube() {
	 
			JFrame frame = new JFrame("Rubik's Cube");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setResizable(false);

			GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration();
			Canvas3D canvas3D = new Canvas3D(config);
			canvas3D.setPreferredSize(new Dimension(800,600));

			scena = utworzScene();
			scena.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
			enablePicking(scena);
			root = new BranchGroup();
			root.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
			root.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
			root.setCapability(BranchGroup.ALLOW_DETACH);
			root.addChild(scena);
			root.compile();
	 
			simpleU = new SimpleUniverse(canvas3D);
	 
			Transform3D przesuniecie_obserwatora = new Transform3D();
			przesuniecie_obserwatora.set(new Vector3f(0.0f,0.0f,3.0f));
	 
			simpleU.getViewingPlatform().getViewPlatformTransform().setTransform(przesuniecie_obserwatora);
	 
			simpleU.addBranchGraph(root);
	 
			OrbitBehavior orbit = new OrbitBehavior(canvas3D, OrbitBehavior.REVERSE_ROTATE);
			orbit.setSchedulingBounds(new BoundingSphere());
			simpleU.getViewingPlatform().setViewPlatformBehavior(orbit);
	 
	 
			frame.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent winEvent) {
					System.exit(0);
				}
			});
	 
			frame.add(canvas3D);
			pickCanvas = new PickCanvas(canvas3D, scena);
			pickCanvas.setMode(PickCanvas.GEOMETRY);
	 
			canvas3D.addMouseListener(this);
			canvas3D.addMouseMotionListener(this);
			canvas3D.addKeyListener(this);
			frame.pack();
			frame.setVisible(true);
		}
		
		public void enablePicking(Node node) {
			node.setPickable(true);
			node.setCapability(Node.ENABLE_PICK_REPORTING);
	 
			try {
				Group group = (Group) node;
		   
				for (Enumeration e = group.getAllChildren(); e.hasMoreElements();) {
					enablePicking((Node)e.nextElement());
				}
			}
			catch(ClassCastException e) {
			// if not a group node, there are no children so ignore exception
			}
	 
			try {
				Shape3D shape = (Shape3D) node;
				PickTool.setCapabilities(node, PickTool.INTERSECT_FULL);
	 
				for (Enumeration e = shape.getAllGeometries(); e.hasMoreElements();) {
					Geometry g = (Geometry)e.nextElement();
					g.setCapability(g.ALLOW_INTERSECT);
				}
		   }
	 
		catch(ClassCastException e) {
		   // not a Shape3D node ignore exception
		}
	}
		

	 
		BranchGroup utworzScene(){
	 
		  BranchGroup wezel_scena = new BranchGroup();
		  wezel_scena.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
		  wezel_scena.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
		  wezel_scena.setCapability(BranchGroup.ALLOW_DETACH);


		  
		  TransformGroup kostki = new TransformGroup();
		  kostki.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		 
		  warstwa.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		  int nr=0;
		  int x=0;
		  int y=0;
		  int z=0;
	 
		  for (double i = -0.101; i < 0.202; i+=0.101) {
			   for (double j = -0.101; j < 0.202; j+=0.101) {  
				   for (double k = -0.101; k < 0.202; k+=0.101) {
						TransformGroup G_kostka = new TransformGroup();
						G_kostka.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
						G_kostka.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
						Transform3D p_kostka = new Transform3D();
						p_kostka.setTranslation(new Vector3d(i,j,k));
						G_kostka.setTransform(p_kostka);
						myCube kos = new myCube(0.04d,nr);
	 
						G_kostka.addChild(kos);
						TransformGroup G_kostka2 = new TransformGroup();
						//SharedGroup G_kostka = new SharedGroup();
						G_kostka2.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
						G_kostka2.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
						G_kostka2.addChild(G_kostka);
						kosteczki.add(G_kostka2);
	 
						kostka[x][y][z] = nr;

						nr++;
						z++;
						
						if(nr == 20) Axis(G_kostka);
					}
					z = 0;
					y++;
				}
			   y = 0;
			   x++;
			}
	 
		 
		   for (TransformGroup elment : kosteczki) {
			   wezel_scena.addChild(elment);
		   }
		   
		   
		   // osie wsp�rz�dnych
		   
	 
	 
		  return wezel_scena;
	 
		}
		
		
		public void Axis(TransformGroup G_kostka){
			Point3d coords[] = new Point3d[2];
			Appearance app=new Appearance();
			int vertexCounts[] = {2};
			coords[0] = new Point3d(0.0d, 0.0d, 0.0d);
			coords[1] = new Point3d(0.5d, 0.0d, 0.0d);

			LineStripArray lines = new LineStripArray(2, GeometryArray.COORDINATES, vertexCounts);
			lines.setCoordinates(0, coords);
			Shape3D shape = new Shape3D(lines , app);
			G_kostka.addChild(shape);

			coords[1] = new Point3d(0.0d, 0.5d, 0.0d);

			lines = new LineStripArray(2, GeometryArray.COORDINATES, vertexCounts);
			lines.setCoordinates(0, coords);
			shape = new Shape3D(lines , app);
			G_kostka.addChild(shape);

			 coords[1] = new Point3d(0.0d, 0.0d, 0.5d);

			lines = new LineStripArray(2, GeometryArray.COORDINATES, vertexCounts);
			lines.setCoordinates(0, coords);
			shape=new Shape3D(lines , app);
			G_kostka.addChild(shape);
		}
			
			
	   public void mouseDragged(MouseEvent e){
		   if(mouseIsMoving){
			   System.out.println(e.getPoint().x);
		   }
	   } 
	   public void mouseReleased(MouseEvent e){
		   mouseIsMoving = false;
	   }
	   
	   public void rotateCube(myCube cube, TransformGroup rotation){
			Transform3D obrCal = new Transform3D();
			Transform3D transformCube = new Transform3D();
	 
			for(int n = cube.katObrotu.size()-1; n >= 0; n--){

				if(cube.ktoraOs.get(n) == 'x')
					transformCube.setRotation(new AxisAngle4d(1,0,0,cube.katObrotu.get(n)));
	 
				else if(cube.ktoraOs.get(n) == 'y')
					transformCube.setRotation(new AxisAngle4d(0,1,0,cube.katObrotu.get(n)));
	 
				else if(cube.ktoraOs.get(n) == 'z')
					transformCube.setRotation(new AxisAngle4d(0,0,1,cube.katObrotu.get(n)));
	 
				obrCal.mul(transformCube);
			}  
			rotation.setTransform(obrCal);
	   }
	   
	   
		public void keyPressed(KeyEvent e) {
			System.out.println("you Clicked me");
	 
			TransformGroup temp;
			TransformGroup tempCube;
			myCube tempC;
	 
			if(e.getKeyChar() == 'r') {
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 3; j++) {
						temp = kosteczki.get(kostka[2][i][j]);
						tempCube = (TransformGroup)(temp.getChild(0));
						tempC = (myCube)tempCube.getChild(0);
						
						tempC.katObrotu.add(-Math.PI/2);
						tempC.ktoraOs.add('x');
						rotateCube(tempC, temp);
					}
				}
				Obrot("R");
			}
		   
			if(e.getKeyChar() == 'u') {
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 3; j++) {
						temp = kosteczki.get(kostka[i][j][2]);
						tempCube = (TransformGroup)(temp.getChild(0));
						tempC = (myCube)tempCube.getChild(0);
	 
						tempC.katObrotu.add(-Math.PI/2);
						tempC.ktoraOs.add('z');
						rotateCube(tempC, temp);
					}
				}
				Obrot("U");
			}
	 
			if(e.getKeyChar() == 'd') {
				for (int i = 0; i < 3; i++) {
					 for (int j = 0; j < 3; j++) {
						temp = kosteczki.get(kostka[i][j][0]);
						tempCube = (TransformGroup)(temp.getChild(0));
						tempC = (myCube)tempCube.getChild(0);

						tempC.katObrotu.add(Math.PI/2);
						tempC.ktoraOs.add('z');
						rotateCube(tempC, temp);
				   }
			   }
			   Obrot("D");
		   }
	 
			if(e.getKeyChar() == 'l') {
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 3; j++) {
						temp = kosteczki.get(kostka[0][i][j]);
						tempCube = (TransformGroup)(temp.getChild(0));
						tempC = (myCube)tempCube.getChild(0);
					   
						tempC.katObrotu.add(Math.PI/2);
						tempC.ktoraOs.add('x');
						rotateCube(tempC, temp);
					}
				}
				Obrot("L");
			}
	 
			if(e.getKeyChar() == 'f') {
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 3; j++) {
						temp = kosteczki.get(kostka[i][0][j]);
						tempCube = (TransformGroup)(temp.getChild(0));
						tempC = (myCube)tempCube.getChild(0);
						
						tempC.katObrotu.add(Math.PI/2);
						tempC.ktoraOs.add('y');
						rotateCube(tempC, temp);
					}
				}
				Obrot("F");
			}

			if(e.getKeyChar() == 'g') {
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 3; j++) {
						temp = kosteczki.get(kostka[i][2][j]);
						tempCube = (TransformGroup)(temp.getChild(0));
						tempC = (myCube)tempCube.getChild(0);
					   
						tempC.katObrotu.add(-Math.PI/2);
						tempC.ktoraOs.add('y');   
						rotateCube(tempC, temp);
					}
				}
				Obrot("G");
			}
		}
		   
		
	   public void mousePressed(MouseEvent e)
		{
			mouseIsMoving = true;
			pickCanvas.setShapeLocation(e);
			PickResult result = pickCanvas.pickClosest();
	 
			if (result == null) {
			   System.out.println("Nothing picked");
			} else {
			   Primitive p = (Primitive)result.getNode(PickResult.PRIMITIVE);
			   myCube s = (myCube)result.getNode(PickResult.SHAPE3D);
	 
			   if (p != null) {
				  System.out.println(p.getClass().getName());
			   } else if (s != null) {
					 s.HowYouAre();
					 if (s.nr == 0){
	//                     kat+=Math.PI/2;
	//                     if(kat==2*Math.PI) kat=0;
	//                     //root.removeChild(scena);
	//                    Transform3D p_kostka = new Transform3D();
	//                    p_kostka.setRotation(new AxisAngle4d(0,1,0,kat));
	//                    warstwa.setTransform(p_kostka);
	//                    Transform3D p_kostka2 = new Transform3D();
	//                    
	////                    kosteczki.get(3).getTransform(p_kostka2);
	////                    p_kostka.mul(p_kostka2);
	//                    kosteczki.get(3).setTransform(p_kostka);
	//                    //warstwa.removeChild(kosteczki.get(0));
	//                    //root.addChild(scena);
					 }
					 if (s.nr == 6){
	//                    if(!zmin)kat2+=Math.PI/20;
	//                    if(kat2==2*Math.PI) kat2=0;
	//                    if(kat2==Math.PI/2) zmin =true;
	//                    if(zmin){
	//                        kat3+=Math.PI/20;
	//                        Transform3D p_kostka = new Transform3D();
	//                    p_kostka.setRotation(new AxisAngle4d(0,1,0,kat3));
	//                    Transform3D p_kostka2 = new Transform3D();
	//                    p_kostka2.setRotation(new AxisAngle4d(0,0,1,kat2));
	//                    //kosteczki.get(6).getTransform(p_kostka2);
	//                    p_kostka.mul(p_kostka2);
	//                    kosteczki.get(6).setTransform(p_kostka);
	//                    return;
	//                    }
	//                     //root.removeChild(scena);
	//                    Transform3D p_kostka = new Transform3D();
	//                    p_kostka.setRotation(new AxisAngle4d(0,0,1,kat2));
	//                    //Transform3D p_kostka2 = new Transform3D();
	//                    
	//                    //kosteczki.get(6).getTransform(p_kostka2);
	//                    //p_kostka.mul(p_kostka2);
	//                    kosteczki.get(6).setTransform(p_kostka);
	//                    //warstwa.removeChild(kosteczki.get(0));
	//                    
	//                    //root.addChild(scena);
					 }
					 System.out.println();
	 
			   } else {
				  System.out.println("null");
			   }
			}
		}
	   
	   
	   public static void main(String args[]){
		  new rubikCube();
	   }
	 
	   
		@Override
		public void keyTyped(KeyEvent e) {
		   // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}
	 
		@Override
		public void keyReleased(KeyEvent e) {
			//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		} 
	 
	}
	 

